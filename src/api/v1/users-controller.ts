import { ExpressController, ExpressJSONCreatePresenter } from '../../modules/core';
import {
    CreateUserRequestObject,
    CreateUserInteractor,
    validationSchema as createSchema,
} from '../../modules/users/create-user-interactor';
import { UserGateway } from '../../modules/users/user-gateway';

export class UsersController extends ExpressController {
    constructor() {
        super();

        this.router.post('/', this.validator.validateBody(createSchema), this.createUser.bind(this));
    }

    async createUser(req, res, next) {
        const userGateway = new UserGateway();

        const interactor = new CreateUserInteractor();

        const presenter = new ExpressJSONCreatePresenter(req, res, next);

        const request = req.body as CreateUserRequestObject;

        interactor.setPresenter(presenter)
            .setUserGateway(userGateway);

        await interactor.execute(request);
    }
}
