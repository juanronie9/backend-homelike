import { ExpressController, ExpressJSONCreatePresenter, ExpressJSONSearchPresenter } from '../../modules/core';
import { AuthMiddleware } from '../../modules/core/auth.middleware';
import { SubscriptionGateway } from '../../modules/subscriptions/subscription-gateway';
import {
    FollowApartmentInteractor,
    FollowApartmentRequestObject,
    validationSchema as createSchema,
} from '../../modules/subscriptions/follow-apartment-interactor';
import {
    GetFollowedApartmentsInteractor,
    GetFollowedApartmentsRequestObject,
    validationSchema as getAllSchema
} from '../../modules/subscriptions/get-followed-apartments-interactor';

export class SubscriptionsController extends ExpressController {
    constructor() {
        super();

        this.router.post('/', AuthMiddleware, this.validator.validateBody(createSchema), this.followApartment.bind(this));
        this.router.get('/', AuthMiddleware, this.validator.validateQuery(getAllSchema), this.getFollowedApartments.bind(this));
    }

    async followApartment(req, res, next) {
        const subscriptionGateway = new SubscriptionGateway();

        const interactor = new FollowApartmentInteractor();

        const presenter = new ExpressJSONCreatePresenter(req, res, next);

        const request = req.body as FollowApartmentRequestObject;
        request.userId = req.userId;

        interactor
            .setPresenter(presenter)
            .setSubscriptionGateway(subscriptionGateway);

        await interactor.execute(request);
    }

    async getFollowedApartments(req, res, next) {
        const subscriptionGateway = new SubscriptionGateway();

        const interactor = new GetFollowedApartmentsInteractor();

        const presenter = new ExpressJSONSearchPresenter(req, res, next);

        const request = req.query as GetFollowedApartmentsRequestObject;
        request.userId = req.userId;

        interactor
            .setPresenter(presenter)
            .setSubscriptionGateway(subscriptionGateway);

        await interactor.execute(request);
    }
}
