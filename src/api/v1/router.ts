import { Router } from 'express';

import { AuthController } from './auth-controller';
import { UsersController } from './users-controller';
import { ApartmentsController } from './apartments-controller';
import { SubscriptionsController } from './subscriptions-controller';

export class ApiRouter {

    constructor() {}

    build() {
        const router = Router();

        const authController = new AuthController();
        const usersController = new UsersController();
        const apartmentsController = new ApartmentsController();
        const subscriptionsController = new SubscriptionsController();

        router.get('/healthcheck', (req, res) => {
            const healthcheck = {
                uptime: process.uptime(),
                message: 'OK',
                timestamp: Date.now()
            };
            try {
                return res.status(200).send(healthcheck);
            } catch (e) {
                healthcheck.message = e;
                return res.status(503).send();
            }
        });

        // OAuth api
        router.use('/oauth', authController.router);

        // Users api
        router.use('/users', usersController.router);

        // Apartments api
        router.use('/apartments', apartmentsController.router);

        // Subscriptions api
        router.use('/subscriptions', subscriptionsController.router);

        return router;
    }
}
