import { ExpressController, ExpressJSONCreatePresenter, ExpressJSONSearchPresenter } from '../../modules/core';
import {
    CreateApartmentRequestObject,
    CreateApartmentInteractor,
    validationSchema as createSchema,
} from '../../modules/apartments/create-apartment-interactor';
import {
    GetApartmentsInteractor,
    GetApartmentsRequestObject,
    validationSchema as getAllSchema
} from '../../modules/apartments/get-apartments-interactor';
import { ApartmentGateway } from '../../modules/apartments/apartment-gateway';
import { AuthMiddleware } from '../../modules/core/auth.middleware';

export class ApartmentsController extends ExpressController {
    constructor() {
        super();

        this.router.post('/', AuthMiddleware, this.validator.validateBody(createSchema), this.createApartment.bind(this));
        this.router.get('/', AuthMiddleware, this.validator.validateQuery(getAllSchema), this.getApartments.bind(this));
    }

    async createApartment(req, res, next) {
        const apartmentGateway = new ApartmentGateway();

        const interactor = new CreateApartmentInteractor();

        const presenter = new ExpressJSONCreatePresenter(req, res, next);

        const request = req.body as CreateApartmentRequestObject;
        request.userId = req.userId;

        interactor
            .setPresenter(presenter)
            .setApartmentGateway(apartmentGateway);

        await interactor.execute(request);
    }

    async getApartments(req, res, next) {
        const apartmentGateway = new ApartmentGateway();

        const interactor = new GetApartmentsInteractor();

        const presenter = new ExpressJSONSearchPresenter(req, res, next);

        const request = req.query as GetApartmentsRequestObject;
        request.userId = req.userId;

        interactor
            .setPresenter(presenter)
            .setApartmentGateway(apartmentGateway);

        await interactor.execute(request);
    }
}
