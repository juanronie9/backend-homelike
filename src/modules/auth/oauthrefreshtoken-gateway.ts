import { IOAuthRefreshTokenModel, OAuthRefreshTokenModel } from '../../db/models/oauthrefreshtoken.model';
import { MongoGateway } from '../common/persistance-gateway';
import { OAuthRefreshToken } from './oauthrefreshtoken';

export class OauthrefreshtokenGateway extends MongoGateway {
    private _oauthRefreshTokenModel = OAuthRefreshTokenModel;

    setOAuthRefreshTokenModel(model: typeof OAuthRefreshTokenModel) {
        this._oauthRefreshTokenModel = model;
        return this;
    }

    async createOAuthRefreshToken(oauthToken: OAuthRefreshToken): Promise<OAuthRefreshToken> {
        const modelValues = {
            ...oauthToken.toModelValues(),
            createdAt: Date.now(),
            updatedAt: Date.now(),
        }

        const created = await this._oauthRefreshTokenModel.create(modelValues);

        return OAuthRefreshToken.fromRaw(created);
    }

    async validateRefreshToken(token: string): Promise<IOAuthRefreshTokenModel> {
        const query = {
            refreshToken: token,
            refreshTokenExpiresAt: { $gte: new Date() }
        };

        const refreshToken = await this._oauthRefreshTokenModel
            .findOne(query)
            .exec();

        if (!refreshToken) {
            return null;
        }

        return refreshToken;
    }
}
