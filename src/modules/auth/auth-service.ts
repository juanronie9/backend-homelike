import bcrypt from 'bcrypt';
import { IUserModel, UserModel } from '../../db/models/user.model';
import { OAuthtokenGateway } from './oauthtoken-gateway';
import { OauthrefreshtokenGateway } from './oauthrefreshtoken-gateway';
import { logger } from '../../app';
import { UserGateway } from '../users/user-gateway';


export class AuthService {

    constructor() {}

    async login(email: string, password: string): Promise<IUserModel> {
        if (!email || !password) {
            throw new Error('Username or password parameter missing');
        }
        email = email.trim().toLowerCase();

        const user = await UserModel.findOne({ 'email': email, password: { $exists: true } });
        if (!user) {
            throw new Error('User not found');
        }

        const isPasswordValid = bcrypt.compareSync(password, user.password);
        if (!isPasswordValid) {
            throw new Error('Password not valid');
        }
        return user;
    }

    async loginRefreshToken(token: string): Promise<IUserModel> {
        if (!token) {
            throw new Error('Token parameter missing');
        }
        const oauthRefreshTokenGateway = new OauthrefreshtokenGateway();
        const oauthRefreshToken = await oauthRefreshTokenGateway.validateRefreshToken(token);
        if (!oauthRefreshToken) {
            throw new Error('Refresh Token not valid');
        }

        const user = await UserModel.findById(oauthRefreshToken.userId);
        if (!user) {
            throw new Error('User not found');
        }
        return user;
    }

    obtainTokenFromRequest(req, headerKeys, queryKey): string {
        let token = '';
        if (req.query && req.query[queryKey]) {
            return req.query[queryKey];
        }
        if (req.headers) {
            for (const headerKey of headerKeys) {
                if (req.headers[headerKey]) {
                    token = req.headers[headerKey];
                    break;
                }
            }
        }
        return token;
    }

    async validateToken(token: string): Promise<boolean> {
        try {
            const oauthTokenGateway = new OAuthtokenGateway();
            const isValid = await oauthTokenGateway.validateAccessToken(token);
            if (!isValid) {
                return false;
            }
            return true;
        } catch (e) {
            logger.error('Error during validate token: ', e.message);
            return false;
        }
    }

    async getUserByToken(token: string): Promise<IUserModel> {
        try {
            const oauthTokenGateway = new OAuthtokenGateway();
            const isValid = await oauthTokenGateway.validateAccessToken(token);
            if (!isValid) {
                return null;
            }

            const userGateway = new UserGateway();
            const user = await userGateway.getUserById(isValid.userId);
            if (!user) {
                return null;
            }
            return user;
        } catch (e) {
            logger.error('Error during validate token: ', e.message);
            return null;
        }
    }
}
