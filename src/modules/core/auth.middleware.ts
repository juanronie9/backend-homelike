import { NextFunction } from 'express';
import { AuthService } from '../auth/auth-service';
import { ExpressAuthPresenter } from './usecase-interactor';

export const TOKEN_HEADER_KEYS = ['access-token','x-auth-token'];
export const TOKEN_QUERY_KEY = 'access_token';

export const AuthMiddleware = async (req, res, next: NextFunction) => {
    const presenter = new ExpressAuthPresenter(req, res, next);
    const authService = new AuthService();
    const token = authService.obtainTokenFromRequest(req, TOKEN_HEADER_KEYS, TOKEN_QUERY_KEY);
    if (!token) {
        return presenter.error(['Token is required']);
    }

    try {
        const isValid = await authService.validateToken(token);
        if (!isValid) {
            return presenter.error(['Token is not valid']);
        }

        const user = await authService.getUserByToken(token);
        if (!user) {
            return presenter.error(['Token is not valid']);
        }
        req.accessToken = token;
        req.userId = user._id;
        return presenter.success(); // next()
    } catch (e) {
        return presenter.error([e.message || e]);
    }
}