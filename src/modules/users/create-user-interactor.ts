import { Interactor, IRequestObject } from '../core';
import joi from 'joi';
import { logger } from '../../app';
import { UserGateway } from './user-gateway';
import { User } from './user';

export interface CreateUserRequestObject extends IRequestObject {
    email: string,
    password: string,
}

export const validationSchema = joi.object().keys({
    email: joi
        .string()
        .email()
        .required(),
    password: joi
        .string()
        .regex(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{8,30}$/)
        .required(),
});

export class CreateUserInteractor extends Interactor {

    private _userGateway: UserGateway = null;

    setUserGateway(gateway: UserGateway) {
        this._userGateway = gateway;
        return this;
    }

    async execute(request: CreateUserRequestObject) {
        try {
            const data = await this._userGateway.getUserByEmail(request.email);
            if (data) {
                return this.presenter.error(['User already exists']);
            }
            const user = new User();
            user.setPassword(request.password);
            user.setEmail(request.email);
            const created = await this._userGateway.createUser(user);
            return this.presenter.success(created);
        } catch (e) {
            logger.error('Failed to create user', request, e);
            return this.presenter.error([e.message]);
        }
    }
}