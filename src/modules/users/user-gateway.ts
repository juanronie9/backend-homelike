import { IUserModel, UserModel } from '../../db/models/user.model';

import { MongoGateway } from '../common/persistance-gateway';
import { /*IUser,*/ User } from './user';

export class UserGateway extends MongoGateway {
    private _userModel = UserModel;

    setUserModel(model: typeof UserModel) {
        this._userModel = model;
        return this;
    }

    async createUser(user: User): Promise<IUserModel> {
        const modelValues = {
            ...user.toModelValues(),
            createdAt: Date.now(),
            updatedAt: Date.now(),
        };

        const created = await this._userModel.create(modelValues);
        return created;
    }

    async getUserByEmail(email: string): Promise<IUserModel>{
        const query = {
            email,
        };
        return UserModel.findOne(query).exec();
    }

    async getUserById(userId: string): Promise<IUserModel>{
        const query = {
            _id: userId
        };
        return UserModel.findById(query).exec();
    }
}
