import { DEFAULT_PAGE_NUM, DEFAULT_PAGE_SIZE } from '../common/pagination';
import { MongoGateway } from '../common/persistance-gateway';
import { Apartment } from './apartment';
import { ApartmentModel, IApartmentModel } from '../../db/models/apartment.model';

export interface GetApartmentsParameters {
    page: number;
    pageSize: number;
    city?: string,
    country?: string,
    rooms?: number
    lat?: number,
    lon?: number
    nearest?: number,
}

export interface GetApartmentsResult {
    count: number;
    rows: IApartmentModel[];
}

export class ApartmentGateway extends MongoGateway {
    private _apartmentModel = ApartmentModel;

    setApartmentModel(model: typeof ApartmentModel) {
        this._apartmentModel = model;
        return this;
    }

    async createApartment(apartment: Apartment): Promise<IApartmentModel> {
        const modelValues = {
            ...apartment.toModelValues(),
            createdAt: Date.now(),
            updatedAt: Date.now(),
        };

        const created = await this._apartmentModel.create(modelValues);
        return created;
    }

    async getApartments({
        page = DEFAULT_PAGE_NUM,
        pageSize = DEFAULT_PAGE_SIZE,
        city,
        country,
        rooms,
        lat,
        lon,
        nearest = 10
    }: GetApartmentsParameters): Promise<GetApartmentsResult> {

        const query: any = {};

        if (city) {
            query.city = city;
        }
        if (country) {
            query.country = country;
        }
        if (rooms) {
            query.rooms = rooms;
        }

        let rows, count;
        if (lat && lon && nearest) {
            rows = await this._apartmentModel.aggregate([
                {
                    $geoNear: {
                        near: { type: 'Point', coordinates: [ lon , lat ] },
                        distanceField: 'dist.calculated',
                        maxDistance: nearest * 1000,
                        query,
                        // includeLocs: 'dist.location',
                        spherical: true
                    },
                },
                { $sort: { 'createdAt': -1 } },
                { $skip: pageSize * (page - 1) },
                { $limit: pageSize }
            ]);
            count = rows.length;
        } else {
             rows = await this._apartmentModel.find(query)
                .sort({ createdAt: 'descending' })
                .skip(pageSize * (page - 1))
                .limit(pageSize);
             count = await this._apartmentModel.count(query);
        }

        return {
            count,
            rows,
        };
    }
}
