import joi from 'joi';
import { IRequestObject, Interactor, PaginationData } from '../core';
import { MAX_PAGE_SIZE, MIN_PAGE_NUMBER, MIN_PAGE_SIZE } from '../common/pagination';
import { logger } from '../../app';
import { ApartmentGateway } from './apartment-gateway';

export interface GetApartmentsRequestObject extends IRequestObject {
    page: number;
    pageSize: number;
    city: string,
    country: string,
    rooms: number,
    lat: number,
    lon: number,
    nearest: number,
}

export const validationSchema = joi.object().keys({
    page: joi
        .number()
        .integer()
        .min(MIN_PAGE_NUMBER)
        .default(MIN_PAGE_NUMBER)
        .optional(),
    pageSize: joi
        .number()
        .integer()
        .min(MIN_PAGE_SIZE)
        .max(MAX_PAGE_SIZE)
        .default(MIN_PAGE_SIZE)
        .optional(),
    city: joi
        .string()
        .optional(),
    country: joi
        .string()
        .optional(),
    rooms: joi
        .number()
        .optional(),
    lat: joi
        .number()
        .optional(),
    lon: joi
        .number()
        .optional(),
    nearest: joi
        .number()
        .optional()
});

export class GetApartmentsInteractor extends Interactor {

    private _apartmentGateway: ApartmentGateway = null;

    setApartmentGateway(gateway: ApartmentGateway) {
        this._apartmentGateway = gateway;
        return this;
    }

    async execute(request: GetApartmentsRequestObject) {
        try {
            const result = await this._apartmentGateway.getApartments(request);
            const pagination = this._buildPaginationData(
                result.count,
                request.pageSize,
                request.page,
            );

            return await this.presenter.success(result, pagination);
        } catch (error) {
            logger.error('Failed to get apartments', {
                errorMessage: error.message,
                ...request
            });
            return this.presenter.error([error]);
        }
    }

    private _buildPaginationData(
        count: number,
        pageSize: number,
        page: number,
    ): PaginationData {
        const totalPages = count > 0 ? Math.ceil(count / pageSize) : 0;

        const paginationData = {
            totalPages: Number(totalPages),
            page,
            pageSize,
        };

        return paginationData;
    }
}
