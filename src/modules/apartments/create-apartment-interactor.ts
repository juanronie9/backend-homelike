import { Interactor, IRequestObject } from '../core';
import joi from 'joi';
import { logger } from '../../app';
import { ApartmentGateway } from './apartment-gateway';
import { Apartment } from './apartment'
import { MapsService } from './maps-service';

export interface CreateApartmentRequestObject extends IRequestObject {
    street: string,
    streetNumber: string,
    city: string,
    country: string,
    zipcode: string,
    rooms: number,
}

export const validationSchema = joi.object().keys({
    street: joi
        .string()
        .required(),
    streetNumber: joi
        .string()
        .required(),
    city: joi
        .string()
        .required(),
    country: joi
        .string()
        .required(),
    zipcode: joi
        .string()
        .required(),
    rooms: joi
        .number()
        .required()
});

export class CreateApartmentInteractor extends Interactor {

    private _apartmentGateway: ApartmentGateway = null;

    setApartmentGateway(gateway: ApartmentGateway) {
        this._apartmentGateway = gateway;
        return this;
    }

    async execute(request: CreateApartmentRequestObject) {
        try {
            // 1. Validate that address exists and get coordinates (lat/lon)
            const mapsService = new MapsService();
            const address = {
                street: request.street,
                streetNumber: request.streetNumber,
                town: request.city,
                postalCode: request.zipcode,
                country: request.country
            }
            const data = await mapsService.getAddress(address);
            if (!data) {
                return this.presenter.error(['Address not found']);
            }

            // 2. Create apartment
            const apartment = Apartment.fromRaw(request);
            const location = {
                type: 'Point',
                coordinates: [data.lon, data.lat]
            };
            apartment.setLocation(location);
            const created = await this._apartmentGateway.createApartment(apartment);
            return this.presenter.success(created);
        } catch (e) {
            logger.error('Failed to create apartment', request, e);
            return this.presenter.error([e.message]);
        }
    }
}