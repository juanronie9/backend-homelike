import { logger } from '../../app';
import axios from 'axios';
import { RedisStorage } from '../common/redis-storage';

const DEFAULT_CACHE_TIME_MS = 12 * 60 * 60 * 1000; // 12h

export interface IGetAddressRequestParameters {
    street: string,
    streetNumber: string,
    town: string;
    postalCode: string;
    country: string;
}

export interface IGetAddressResponse {
    name: string,
    lat: number,
    lon: number,
    type: string,
}

export class MapsService {

    private _redisStorage;

    constructor() {
        this._redisStorage = RedisStorage.getInstance();
    }

    async getAddress({ street, streetNumber, town, postalCode, country }: IGetAddressRequestParameters): Promise<IGetAddressResponse> {
        const key = `${country}_${postalCode}_${town}_${street}_${streetNumber}`;
        const base64key = Buffer.from(key).toString('base64');
        const cachedAddress: IGetAddressResponse = await this._redisStorage.getObject(`bw:address:${base64key}`);
        if (cachedAddress) {
            return cachedAddress;
        }

        const uri = `https://nominatim.openstreetmap.org/search`;
        const options = {
            method: 'GET',
            uri,
            params: {
                'street': `${ streetNumber } ${ street }`,
                'city': town,
                'country': country,
                'postalcode': postalCode,
                'format': 'json'
            },
            json: true,
        };

        try {
            const result = await axios.get(options.uri, { params: options.params });
            if (result.data.length === 0) {
                return null;
            }
            const data = result.data[0];
            const obj: IGetAddressResponse = {
                name: data.display_name,
                lat: Number(data.lat),
                lon: Number(data.lon),
                type: data.type
            }

            await this._redisStorage.setObject(`bw:address:${base64key}`, obj, DEFAULT_CACHE_TIME_MS);

            return obj;
        } catch (error) {
            logger.error('Error during address obtaining: ', error.status, error.message);
            throw new Error(`Unable to obtain address. ErrorCode: ${error.status}`);
        }
    }
}
