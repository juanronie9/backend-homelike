import { IPersistableEntity, PersistableEntity } from '../common';
import { ApartmentModel } from '../../db/models/apartment.model';

export interface IApartment extends IPersistableEntity {
    userId: KeyValueHash;
    street: string,
    streetNumber: string,
    city: string,
    country: string,
    zipcode: string,
    rooms: number,
    location: KeyValueHash,
}

export class Apartment extends PersistableEntity implements IApartment {
    private _userId: KeyValueHash;
    private _street: string;
    private _streetNumber: string;
    private _city: string;
    private _country: string;
    private _zipcode: string;
    private _rooms: number;
    private _location: KeyValueHash;

    get userId(): KeyValueHash {
        return this._userId;
    }

    setUserId(value): Apartment {
        this._userId = value;
        return this;
    }

    get street(): string {
        return this._street;
    }

    setStreet(value: string): Apartment {
        this._street = value;
        return this;
    }

    get streetNumber(): string {
        return this._streetNumber;
    }

    setStreetNumber(value: string): Apartment {
        this._streetNumber = value;
        return this;
    }

    get city(): string {
        return this._city;
    }

    setCity(value: string): Apartment {
        this._city = value;
        return this;
    }

    get country(): string {
        return this._country;
    }

    setCountry(value: string): Apartment {
        this._country = value;
        return this;
    }

    get zipcode(): string {
        return this._zipcode;
    }

    setZipcode(value: string): Apartment {
        this._zipcode = value;
        return this;
    }

    get rooms(): number {
        return this._rooms;
    }

    setRooms(value: number): Apartment {
        this._rooms = value;
        return this;
    }

    get location(): object {
        return this._location;
    }

    setLocation(value: KeyValueHash): Apartment {
        this._location = value;
        return this;
    }

    toJSON() {
        const base = super.toJSON();

        return {
            ...base,
            userId: this._userId,
            street: this._street,
            streetNumber: this._streetNumber,
            city: this._city,
            country: this._country,
            zipcode: this._zipcode,
            rooms: this._rooms,
            location: this._location,
        };
    }

    toModelValues() {
        const raw = this.toJSON();

        for (const key of Object.keys(raw)) {
            if (!raw[key]) delete raw[key];
        }
        return raw;
    }

    static fromRaw(
        raw: KeyValueHash | typeof ApartmentModel | any,
        options?: KeyValueHash,
    ): Apartment {
        const instance = new this();

        instance
            .setId(raw.id)
            .setName(raw.name)
            .setCreatedAt(raw.createdAt)
            .setUpdatedAt(raw.updatedAt)
            .setUserId(raw.userId)
            .setStreet(raw.street)
            .setStreetNumber(raw.streetNumber)
            .setCity(raw.city)
            .setCountry(raw.country)
            .setZipcode(raw.zipcode)
            .setRooms(raw.rooms)
            .setLocation(raw.location)

        return instance;
    }
}
