import { Interactor, IRequestObject } from '../core';
import joi from 'joi';
import { logger } from '../../app';
import { SubscriptionGateway } from './subscription-gateway';

export interface FollowApartmentRequestObject extends IRequestObject {
    userId: string;
    apartmentId: string;
}

export const validationSchema = joi.object().keys({
    apartmentId: joi.string().required(),
});

export class FollowApartmentInteractor extends Interactor {
    private _subscriptionGateway: SubscriptionGateway = null;

    setSubscriptionGateway(gateway: SubscriptionGateway): FollowApartmentInteractor {
        this._subscriptionGateway = gateway;

        return this;
    }

    async execute(request: FollowApartmentRequestObject): Promise<boolean> {
        try {
            const result = await this._subscriptionGateway.followApartment(request.userId, request.apartmentId);
            if (!result) {
                return this.presenter.error(['Failed to follow apartment']);
            }

            return this.presenter.success(result);
        } catch (error) {
            logger.error('Failed to follow apartment', request, error);
            return this.presenter.error([error.message]);
        }
    }
}
