import { MongoGateway } from '../common/persistance-gateway';
import { ISubscriptionModel, SubscriptionModel } from '../../db/models/subscription.model';
import { UserModel } from '../../db/models/user.model';
import { ApartmentModel } from '../../db/models/apartment.model';
import { DEFAULT_PAGE_NUM, DEFAULT_PAGE_SIZE } from '../common';

export interface GetFollowedApartmentsParameters {
    page: number;
    pageSize: number;
    userId: string;
}

export interface GetFollowedApartmentsResult {
    count: number;
    rows: ISubscriptionModel[];
}

export class SubscriptionGateway extends MongoGateway {
    private _subscriptionModel = SubscriptionModel;

    setSubscriptionModel(model: typeof SubscriptionModel) {
        this._subscriptionModel = model;
        return this;
    }

    async followApartment(userId, apartmentId): Promise<ISubscriptionModel> {
        const apartment = await ApartmentModel.findById(apartmentId);
        if (!apartment) {
            return null;
        }
        const user = await UserModel.findById(userId);
        if (!user) {
            return null;
        }

        const query = {
            userId: user._id,
            apartmentId: apartment._id,
        };
        const subscription = await this._subscriptionModel.findOne(query);
        if (subscription) {
            return subscription;
        }

        const obj = {
            userId: user._id,
            apartmentId: apartment._id,
            createdAt: Date.now(),
            updatedAt: Date.now(),
        }
        const createdSubscription = await this._subscriptionModel.create(obj);
        return createdSubscription;
    }

    async getFollowedApartments({
                            page = DEFAULT_PAGE_NUM,
                            pageSize = DEFAULT_PAGE_SIZE,
                            userId
                        }: GetFollowedApartmentsParameters): Promise<GetFollowedApartmentsResult> {

        const user = await UserModel.findById(userId);
        if (!user) {
            return {
                count: 0,
                rows: []
            };
        }

        const query = {
            userId: user._id,
        };
        const rows = await this._subscriptionModel.find()
            .populate('apartmentId')
            .sort({ createdAt: 'descending' })
            .skip(pageSize * (page - 1))
            .limit(pageSize);
        const count = await this._subscriptionModel.count(query);

        return {
            count,
            rows,
        };
    }
}
