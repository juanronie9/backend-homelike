import joi from 'joi';
import { IRequestObject, Interactor, PaginationData } from '../core';
import { MAX_PAGE_SIZE, MIN_PAGE_NUMBER, MIN_PAGE_SIZE } from '../common/pagination';
import { logger } from '../../app';
import { SubscriptionGateway } from './subscription-gateway';

export interface GetFollowedApartmentsRequestObject extends IRequestObject {
    page: number;
    pageSize: number;
    userId: string;
}

export const validationSchema = joi.object().keys({
    page: joi
        .number()
        .integer()
        .min(MIN_PAGE_NUMBER)
        .default(MIN_PAGE_NUMBER)
        .optional(),
    pageSize: joi
        .number()
        .integer()
        .min(MIN_PAGE_SIZE)
        .max(MAX_PAGE_SIZE)
        .default(MIN_PAGE_SIZE)
        .optional(),
});

export class GetFollowedApartmentsInteractor extends Interactor {

    private _subscriptionGateway: SubscriptionGateway = null;

    setSubscriptionGateway(gateway: SubscriptionGateway) {
        this._subscriptionGateway = gateway;
        return this;
    }

    async execute(request: GetFollowedApartmentsRequestObject) {
        try {
            const result = await this._subscriptionGateway.getFollowedApartments(request);
            const pagination = this._buildPaginationData(
                result.count,
                request.pageSize,
                request.page,
            );

            return await this.presenter.success(result, pagination);
        } catch (error) {
            logger.error('Failed to get followed apartments', {
                errorMessage: error.message,
                ...request
            });
            return this.presenter.error([error]);
        }
    }

    private _buildPaginationData(
        count: number,
        pageSize: number,
        page: number,
    ): PaginationData {
        const totalPages = count > 0 ? Math.ceil(count / pageSize) : 0;

        const paginationData = {
            totalPages: Number(totalPages),
            page,
            pageSize,
        };

        return paginationData;
    }
}
