import mongoose, { Document, Schema } from 'mongoose';
import { baseEntitySchema } from './base-entity-schema';

export interface ISubscriptionModel extends Document {
    _id: string;
    userId: string;
    apartmentId: string;
    createdAt: Date;
    updatedAt: Date;
}

export const subscriptionSchema = new mongoose.Schema(
    {
        ...baseEntitySchema,
        userId: { type: Schema.Types.ObjectId, ref: 'User' },
        apartmentId: { type: Schema.Types.ObjectId, ref: 'Apartment' },
    },
    { timestamps: true },
);

export const SubscriptionModel = mongoose.model<ISubscriptionModel>('Subscription', subscriptionSchema);