import mongoose, { Document, Schema } from 'mongoose';
import { baseEntitySchema } from './base-entity-schema';

export interface IOAuthTokenModel extends Document {
    _id: string;
    name: string;
    accessToken: string;
    accessTokenExpiresAt: Date;
    oauthClientId: string;
    userId: string;
    scopes: string[];
    createdAt: Date;
    updatedAt: Date;
}

export const oauthTokenSchema = new mongoose.Schema(
    {
        ...baseEntitySchema,
        accessToken: String,
        accessTokenExpiresAt: Date,
        oauthClientId: { type: Schema.Types.ObjectId, ref: 'OAuthClient' },
        userId: { type: Schema.Types.ObjectId, ref: 'User' },
        scopes: [String],
    },
    { timestamps: true },
);

export const OAuthtokenModel = mongoose.model<IOAuthTokenModel>('OAuthToken', oauthTokenSchema);
