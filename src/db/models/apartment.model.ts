import mongoose, { Document, Schema } from 'mongoose';
import { baseEntitySchema } from './base-entity-schema';

export interface ILocationType {
    type: string,
    coordinates: [Number],
    index: string,
}

export interface IApartmentModel extends Document {
    _id: string;
    name: string;
    userId: string;
    street: string;
    streetNumber: string;
    city: string;
    country: string;
    zipcode: string;
    rooms: number;
    location: ILocationType,
    createdAt: Date;
    updatedAt: Date;
}

const pointSchema = new mongoose.Schema({
    type: {
        type: String,
        enum: ['Point'],
        required: true
    },
    coordinates: {
        type: [Number],
        required: true
    }
});

export const apartmentSchema = new mongoose.Schema(
    {
        ...baseEntitySchema,
        userId: { type: Schema.Types.ObjectId, ref: 'User' },
        street: String,
        streetNumber: String,
        city: String,
        country: String,
        zipcode: String,
        rooms: Number,
        location: {
            type: pointSchema,
            index: '2dsphere'
        },
    },
    { timestamps: true },
);

export const ApartmentModel = mongoose.model<IApartmentModel>('Apartment', apartmentSchema);
