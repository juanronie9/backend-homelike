import { OAuthclientModel } from './oauthclient.model';
import { OAuthtokenModel } from './oauthtoken.model';
import { UserModel } from './user.model';

export default {
    OAuthclientModel,
    OAuthtokenModel,
    UserModel,
};
