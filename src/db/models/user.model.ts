import mongoose, { Document, Schema } from 'mongoose';
import { baseEntitySchema } from './base-entity-schema';

export interface IUserModel extends Document {
    _id: string;
    name: string;
    createdAt: Date;
    updatedAt: Date;
    email: string;
    password: string;
    oauthclients: any[];
}

export const userSchema = new mongoose.Schema(
    {
        ...baseEntitySchema,
        email: String,
        password: String,
        oauthclients: [{ type: Schema.Types.ObjectId, ref: 'OAuthClient' }],
    },
    { timestamps: true },
);

export const UserModel = mongoose.model<IUserModel>('User', userSchema);
