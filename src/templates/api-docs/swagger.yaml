openapi: 3.0.3
info:
  title: Backend Service
  description: Provides auth
  version: 0.0.0
servers:
  - url: http://0.0.0.0:3000/v1
paths:
  /oauth/token:
    post:
      tags:
        - oauth
      summary: obtain access_token
      operationId: /v1/getAccessToken
      requestBody:
        content:
          application/json:
            schema:
              allOf:
                - $ref: '#/components/schemas/getOAuthTokenRequest'
            examples:
              password:
                summary: grant_type password
                value:
                  client_id: 2_3nio9v1agigw844kssgk4kswksgs0wk4okkoksg4080osoos84
                  client_secret: 4394ff69idq8g08k4wso4ck8008w0g44ck88wo8c8cw80wk8c4
                  grant_type: password
                  username: test@test.com
                  password: Asdf1234
              refresh_token:
                summary: grant_type refresh_token
                value:
                  client_id: 2_3nio9v1agigw844kssgk4kswksgs0wk4okkoksg4080osoos84
                  client_secret: 4394ff69idq8g08k4wso4ck8008w0g44ck88wo8c8cw80wk8c4
                  grant_type: refresh_token
                  refresh_token: 4fdba6c4-b24b-4e96-95c3-dd4f79105bbc
        required: true
      responses:
        200:
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/getOAuthTokenResponse'
        400:
          description: Bad Request
          content: {}
      deprecated: false
      x-codegen-request-body-name: Body
  /apartments:
    post:
      tags:
        - apartments
      summary: create apartment
      operationId: /v1/createApartment
      parameters:
        - name: access-token
          in: header
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/createApartmentRequest'
        required: true
      responses:
        201:
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/createApartmentResponse'
        400:
          description: Bad Request
          content: {}
        404:
          description: Not found
          content: {}
      deprecated: false
      x-codegen-request-body-name: Body
    get:
      tags:
        - apartments
      summary: get apartments
      operationId: /v1/getApartments
      parameters:
        - name: access-token
          in: header
          required: true
          schema:
            type: string
        - name: page
          in: query
          description: page number
          required: false
          schema:
            type: number
        - name: pageSize
          in: query
          description: page size number
          required: false
          schema:
            type: number
        - name: lon
          in: query
          description: longitud
          required: false
          schema:
            type: string
        - name: lat
          in: query
          description: latitud
          required: false
          schema:
            type: string
        - name: nearest
          in: query
          description: nearest to coordinates (lon, lat). Example 10 KMs,  20KMs
          required: false
          schema:
            type: number
            example: 10
      responses:
        200:
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/getApartmentsResponse'
        400:
          description: Bad Request
          content: {}
        404:
          description: Not found
          content: {}
      deprecated: false
      x-codegen-request-body-name: Body

  /subscriptions:
    post:
      tags:
        - subscriptions
      summary: create subscription (favorite)
      operationId: /v1/followApartment
      parameters:
        - name: access-token
          in: header
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/followApartmentRequest'
        required: true
      responses:
        201:
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/followApartmentResponse'
        400:
          description: Bad Request
          content: {}
        404:
          description: Not found
          content: {}
      deprecated: false
      x-codegen-request-body-name: Body
    get:
      tags:
        - subscriptions
      summary: get subscriptions (favorites)
      operationId: /v1/getFollowedApartments
      parameters:
        - name: access-token
          in: header
          required: true
          schema:
            type: string
        - name: page
          in: query
          description: page number
          required: false
          schema:
            type: number
        - name: pageSize
          in: query
          description: page size number
          required: false
          schema:
            type: number
      responses:
        200:
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/getFollowedApartmentsResponse'
        400:
          description: Bad Request
          content: {}
        404:
          description: Not found
          content: {}
      deprecated: false
      x-codegen-request-body-name: Body

  /users:
    post:
      tags:
        - users
      summary: create user
      operationId: /v1/createUser
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/createUserRequest'
        required: true
      responses:
        201:
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/createUserResponse'
        400:
          description: Bad Request
          content: {}
        404:
          description: Not found
          content: {}
      deprecated: false
      x-codegen-request-body-name: Body
components:
  schemas:
    getOAuthTokenResponse:
      type: object
      properties:
        access_token:
          type: string
          example: 7a2b2594-c0ae-4ace-8f12-2c885961dbb6
        expires_in:
          type: integer
          example: 36000
        refresh_token:
          type: string
          example: c0c19e43-7bed-4289-93f2-01d1f71523c4
    getOAuthTokenRequest:
      required:
        - client_id
        - client_secret
        - grant_type
      type: object
      properties:
        client_id:
          type: string
          example: 1_3nio9v1agigw844kssgk4kswksgs0wk4okkoksg4080osoos84
        client_secret:
          type: string
          example: 4394ff69idq8g08k4wso4ck8008w0g44ck88wo8c8cw80wk8c4
        grant_type:
          type: string
          example: password
        username:
          type: string
          example: test@test.com
        password:
          type: string
          example: Asdf1234
    createApartmentResponse:
      type: object
      properties:
        _id:
          type: string
          example: 61921b41212a8e1a43e7ddea
        userId:
          type: string
          example: 618cdd5aaed15a03a1badbed
        street:
          type: string
          example: Alcalá
        streetNumber:
          type: string
          example: 42
        city:
          type: string
          example: Madrid
        country:
          type: string
          example: spain
        zipcode:
          type: string
          example: 28014
        rooms:
          type: number
          example: 2
        location:
          type: object
          properties:
            coordinates:
              type: array
              items:
                type: number
              example: [ -3.696513838467072, 40.4182074]
            _id:
              type: string
              example: 61921b41212a8e1a43e7ddeb
            type:
              type: string
              example: Point
        createdAt:
          type: string
          format: date-time
        updatedAt:
          type: string
          format: date-time
    createApartmentRequest:
      required:
        - street
        - streetNumber
        - city
        - country
        - zipcode
        - rooms
      type: object
      properties:
        street:
          type: string
          example: Alcalá
        streetNumber:
          type: string
          example: "42"
        city:
          type: string
          example: Madrid
        country:
          type: string
          example: spain
        zipcode:
          type: string
          example: "28014"
        rooms:
          type: number
          example: 2

    getApartmentsResponse:
      type: object
      properties:
        count:
          type: number
          example: 1
        rows:
          type: array
          items:
            type: object
            properties:
              _id:
                type: string
                example: 61921b41212a8e1a43e7ddea
              userId:
                type: string
                example: 618cdd5aaed15a03a1badbed
              street:
                type: string
                example: Alcalá
              streetNumber:
                type: string
                example: 42
              city:
                type: string
                example: Madrid
              country:
                type: string
                example: spain
              zipcode:
                type: string
                example: 28014
              rooms:
                type: number
                example: 2
              location:
                type: object
                properties:
                  coordinates:
                    type: array
                    items:
                      type: number
                    example: [ -3.696513838467072, 40.4182074]
                  _id:
                    type: string
                    example: 61921b41212a8e1a43e7ddeb
                  type:
                    type: string
                    example: Point
              createdAt:
                type: string
                format: date-time
              updatedAt:
                type: string
                format: date-time
              dist:
                type: object
                properties:
                  calculated:
                    type: number
                    example: 24025.621460263967

    followApartmentResponse:
      type: object
      properties:
        _id:
          type: string
          example: 618e4fedb25ca117fbb94813
        userId:
          type: string
          example: 618cdd5aaed15a03a1badbed
        apartmentId:
          type: string
          example: 618d27134abcea63593c1474
        createdAt:
          type: string
          format: date-time
        updatedAt:
          type: string
          format: date-time
    followApartmentRequest:
      required:
        - apartmentId
      type: object
      properties:
        apartmentId:
          type: string
          example: 618d27134abcea63593c1474


    getFollowedApartmentsResponse:
      type: object
      properties:
        count:
          type: number
          example: 1
        rows:
          type: array
          items:
            type: object
            properties:
              _id:
                type: string
                example: 61921b41212a8e1a43e7ddea
              userId:
                type: string
                example: 618cdd5aaed15a03a1badbed
              apartmentId:
                type: object
                properties:
                  _id:
                    type: string
                    example: 61921b41212a8e1a43e7ddea
                  street:
                    type: string
                    example: Alcalá
                  streetNumber:
                    type: string
                    example: 42
                  city:
                    type: string
                    example: Madrid
                  country:
                    type: string
                    example: spain
                  zipcode:
                    type: string
                    example: 28014
                  rooms:
                    type: number
                    example: 2
                  location:
                    type: object
                    properties:
                      coordinates:
                        type: array
                        items:
                          type: number
                        example: [ -3.696513838467072, 40.4182074]
                      _id:
                        type: string
                        example: 61921b41212a8e1a43e7ddeb
                      type:
                        type: string
                        example: Point
                  createdAt:
                    type: string
                    format: date-time
                  updatedAt:
                    type: string
                    format: date-time
              createdAt:
                type: string
                format: date-time
              updatedAt:
                type: string
                format: date-time


    createUserResponse:
      type: object
      properties:
        _id:
          type: string
          example: 6192186b212a8e1a43e7dddf
        email:
          type: string
          example: test@test.com
        createdAt:
          type: string
          format: date-time
        updatedAt:
          type: string
          format: date-time
    createUserRequest:
      required:
        - email
        - password
      type: object
      properties:
        email:
          type: string
          example: test@test.com
        password:
          type: string
          example: Asdf1234
