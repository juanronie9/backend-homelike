# The Backend API

Provides a rich `backend-api` abilities.

## API

API is built on top of [OpenAPI 3.0 specification](https://swagger.io/specification/)
See: [swagger.yml](./src/templates/api-docs/swagger.yaml) for details.

#### Documentation
```
# DEV
http://localhost:3000/docs/api

# PROD
https://<domain>/docs/api
```

## Building

The project uses [TypeScript](https://github.com/Microsoft/TypeScript) as a JavaScript transpiler.
The sources are located under the `src` directory, the distributables are in the `dist`.

### Requirements:

-   Nodejs
-   MongoDB
-   Redis

### Environment:

```bash
APP_HTTP_PORT=3000
APP_PUBLIC_URL=http://localhost:3000
APP_ENVIRONMENT_NAME=
APP_MONGO_DATABASE=
APP_MONGO_HOST=
MONGO_PASS=
MONGO_PORT=27017
MONGO_SRV=
APP_MONGO_USER=
APP_ADMIN_ACCESS_IP_LIST=
APP_REDIS_HOST=
APP_REDIS_PORT=6379
```

To make the application running. Steps:

0. Create database in your MongoDB host `homelike`
1. Rename file `.env.example` to `.env`
2. Set Environment
    ```
    set -a
    . .env
    set +a
    ```
3. Init application
    ```bash
    npm run build
    npm run start
    ```
4. Create in mongodb a new oauth client. Example:
    ```
    db.oauthclients.insertOne({ clientId: "2_3nio9v1agigw844kssgk4kswksgs0wk4okkoksg4080osoos84", clientSecret: "4394ff69idq8g08k4wso4ck8008w0g44ck88wo8c8cw80wk8c4", allowedGrants: ["password", "refresh_token"] });
    ```
5. Ready! You can test all APIs available. Example from api docs Swagger
    ```
    http://0.0.0.0:3000/docs/api
    ```




## Development

```bash
npm run debug
```

## Testing

It uses Jest and supertest.
```bash
npm run test
```
Open one terminal with project running and other terminal to execute the e2e api tests.
```bash
npm run test:e2e
```
---

## Docker
Init app with docker-compose. Steps:

1. Rename file `.env.docker-compose-example` to `.env`
2. Set Environment
    ```
    set -a
    . .env
    set +a
    ```
3. Init application
    ```
    docker-compose up -d
    ```
4. Create in mongodb a new oauth client. Example:
    ```
    db.oauthclients.insertOne({ clientId: "2_3nio9v1agigw844kssgk4kswksgs0wk4okkoksg4080osoos84", clientSecret: "4394ff69idq8g08k4wso4ck8008w0g44ck88wo8c8cw80wk8c4", allowedGrants: ["password", "refresh_token"] });
    ```
5. Ready! You can test all APIs available. Example from api docs Swagger
    ```
    http://0.0.0.0:3000/docs/api
    ```
   
---
Execute tests
```
docker-compose exec api npm run test:e2e
docker-compose exec api npm run test
```
Rebuild if some change
```
docker-compose down -v
docker-compose build --no-cache
docker-compose up -d
```

## Application Structure
- [src](./src): The main `backend` application code.
    - [api](./src/api): Defines API routers and controllers
    - [config](./src/config): Defines configuration. Examples ENV_VARS
    - [db](./src/db): MongoDB models and schemas
    - [modules](./src/modules): Defines modules and logic of the application. 
      All the modules are composed of `Interactors` used by controllers, `Gateways` to interact with DB and `Services` to handle logic or request external REST API's.   
- [test](./test): Includes all the testing of the application
- [app.ts](./app.ts): Main file. Inject packages, initiate and bootstrap application


---

# OAuth Grant Types used

### OAuth Resource Owner Password Flow 
For users registered in our database with username and password. Allows exchanging the usename and password for an access token.
```
curl -X POST \
  http://0.0.0.0:3000/v1/oauth/token \
  -H 'content-type: application/json' \
  -d '{
    "client_id": "CLIENT_ID",
    "client_secret": "CLIENT_SECRET",
    "grant_type": "password"
    "username": "USER_EMAIL",
    "password: "USER_PASSWORD"
}'
```
```
{
    "access_token": "7a2b2594-c0ae-4ace-8f12-2c885961dbb6",
    "expires_in": 36000,
    "refresh_token": "c0c19e43-7bed-4289-93f2-01d1f71523c4"
}
```
## OAuth Refresh Token Flow
Allows exchanging the refresh tokens for an access token.
```
curl -X POST \
  http://0.0.0.0:3000/v1/oauth/token \
  -H 'content-type: application/json' \
  -d '{
    "client_id": "CLIENT_ID",
    "client_secret": "CLIENT_SECRET",
    "grant_type": "refresh_token"
    "refresh_token": "REFRESH_TOKEN",
}'
```
```
{
    "access_token": "7a2b2594-c0ae-4ace-8f12-2c885961dbb6",
    "expires_in": 36000,
    "refresh_token": "c0c19e43-7bed-4289-93f2-01d1f71523c4"
}
```