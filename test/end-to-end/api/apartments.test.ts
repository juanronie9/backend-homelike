import supertest from 'supertest';
import _ from 'lodash';
import { MongoConnector } from '../../../src/modules/common/mongo-connector';
import applicationConfig from '../../../src/config/application';
import { DBHelper } from '../../helpers/dbHelper';
import { ApartmentModel, IApartmentModel } from '../../../src/db/models/apartment.model';
import { GetApartmentsParameters } from '../../../src/modules/apartments/apartment-gateway';
import { GetApartmentsRequestObject } from '../../../src/modules/apartments/get-apartments-interactor';
import mongoose from 'mongoose';

const prefix = '/v1/';

function api(path) {
    return prefix + path;
}

let request;

const bodyRequest = {
    street: 'Alcalá',
    streetNumber: '42',
    city: 'Madrid',
    country: 'spain',
    zipcode: '28014',
    rooms: 2,
}
const bodyRequestAuth = {
    client_id: '2_3nio9v1agigw844kssgk4kswksgs0wk4okkoksg4080osoos84',
    client_secret: '4394ff69idq8g08k4wso4ck8008w0g44ck88wo8c8cw80wk8c4',
    grant_type: 'password',
    username: 'test@test.com',
    password: 'Asdf1234'
};
const credentials = {
    'access-token': '',
};
const obj = {
    userId: new mongoose.Types.ObjectId(),
    street: 'Alcalá',
    streetNumber: '42',
    city: 'Madrid',
    country: 'Spain',
    zipcode: '28014',
    rooms: 2,
    location: {
        type: 'Point',
        coordinates: [-3.696513838467072, 40.4182074]
    }
};

// NOTE: Simulated data, in a real application use this flow for every test:
// 1. Insert data in db
// 2. Get data from db
// 3. Verify data from db
// 4. Clear data from db

describe('MapsApiTest', () => {
    beforeAll(async () => {
        const connector = new MongoConnector();
        connector.setConfig(applicationConfig.mongoDb);
        await connector.connect();

        // INIT DATA
        const dbHelper = new DBHelper();
        await dbHelper.createOAuthClient(bodyRequestAuth.client_id, bodyRequestAuth.client_secret);
        await dbHelper.createUser(bodyRequestAuth.username, bodyRequestAuth.password);

        request = supertest('http://0.0.0.0:3000');
        const response = await request
            .post(api('oauth/token'))
            .send(bodyRequestAuth);

        credentials['access-token'] = response.body.access_token;
    });
    afterAll(async () => {
    });

    describe('Create apartment [POST] /v1/apartments', () => {
        it('should fail when credentials are not provided', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            const badCredentials = _.clone(credentials);
            badCredentials['access-token'] = '';
            const response = await request
                .post(api('apartments'))
                .set(badCredentials)
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(401);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });
        it('should fail when body is not provided', async () => {
            const response = await request
                .post(api('apartments'))
                .set(credentials)
                .send({});

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when body is not valid: parameters required missing', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            delete bodyRequestTest.country;
            const response = await request
                .post(api('apartments'))
                .set(credentials)
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when body is not valid: parameters type not valid', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            bodyRequestTest.country = 1234;
            const response = await request
                .post(api('apartments'))
                .set(credentials)
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when data is not valid: country', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            bodyRequestTest.country = 'value_not_valid';
            const response = await request
                .post(api('apartments'))
                .set(credentials)
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should success', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            const response = await request
                .post(api('apartments'))
                .set(credentials)
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(201);
            const { body } = response;
            expect(body._id).toBeDefined();
            expect(body.userId).toBeDefined();

            expect(body.street).toBe(bodyRequestTest.street);
            expect(body.streetNumber).toBe(bodyRequestTest.streetNumber);
            expect(body.city).toBe(bodyRequestTest.city);
            expect(body.country).toBe(bodyRequestTest.country);
            expect(body.zipcode).toBe(bodyRequestTest.zipcode);
            expect(body.rooms).toBe(bodyRequestTest.rooms);
            expect(body.location.coordinates.length).toBeGreaterThanOrEqual(2);
            expect(body.location.type).toBe('Point');

            // 1. Verify apartment exist
            const apartment: IApartmentModel = await ApartmentModel.findById(body._id).exec();
            expect(apartment).toBeDefined();
            expect(apartment._id).not.toBeNull();
            expect(apartment.userId).not.toBeNull();
        });
    });


    describe('Get Apartments [GET] /v1/apartments', () => {
        it('should fail when credentials are not provided', async () => {
            const badCredentials = _.clone(credentials);
            badCredentials['access-token'] = '';
            const response = await request
                .get(api('apartments'))
                .set(badCredentials)
                .send();

            expect(response.statusCode).toBe(401);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        const data0: GetApartmentsParameters = {
            page: 999,
            pageSize: 10,
        };
        const data1: GetApartmentsParameters = {
            page: 1,
            pageSize: 10,
            city: obj.city,
            country: obj.country,
            rooms: obj.rooms,
        };
        const data2: GetApartmentsParameters = {
            page: 1,
            pageSize: 10,
            lon: obj.location.coordinates[0],
            lat: obj.location.coordinates[1],
            nearest: 10
        };
        const data3: GetApartmentsParameters = {
            page: 1,
            pageSize: 10,
            lon: -9.9,
            lat: +9.9,
            nearest: 10
        };
        const data4: GetApartmentsParameters = {
            page: 1,
            pageSize: 10,
            city: obj.city,
            country: obj.country,
            rooms: obj.rooms,
            lon: obj.location.coordinates[0],
            lat: obj.location.coordinates[1],
            nearest: 10
        };

        const testData = [
            [data0, 0, 0],
            [data1, 1, 1],
            [data2, 1, 1],
            [data3, 0, 0],
            [data4, 0, 0],
        ];
        test.each(testData)(
            'should return data. Result for getApartments=%s count=%s  rows=%s',
            async (getApartmentsParameters: GetApartmentsRequestObject, count: number, rows: number) => {
                const bodyRequestTest = _.clone(bodyRequest);
                const responseCreate = await request.post(api('apartments')).set(credentials).send(bodyRequestTest);
                expect(responseCreate.statusCode).toBe(201);

                // 2. Get Data
                const response = await request.get(api('apartments')).set(credentials).query(bodyRequestTest);
                expect(response.statusCode).toBe(200);
                const { body } = response;
                expect(body.count).toBeGreaterThanOrEqual(count);
                expect(body.rows.length).toBeGreaterThanOrEqual(rows);
            });
    });
});
