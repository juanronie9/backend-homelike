import { MongoConnector } from '../../../src/modules/common/mongo-connector';
import applicationConfig from '../../../src/config/application';
import supertest from 'supertest';
import _ from 'lodash';
import faker from 'faker';
import { IUserModel, UserModel } from '../../../src/db/models/user.model';

const prefix = '/v1/';

function api(path) {
    return prefix + path;
}

let request;

const bodyRequest = {
    email: 'test@test.com',
    password: 'Asdf1234'
};

// NOTE: Simulated data, in a real application use this flow for every test:
// 1. Insert data in db
// 2. Get data from db
// 3. Verify data from db
// 4. Clear data from db

describe('UserApiTest', () => {
    beforeAll(async () => {
        const connector = new MongoConnector();
        connector.setConfig(applicationConfig.mongoDb);
        await connector.connect();

        request = supertest('http://0.0.0.0:3000');
    });
    afterAll(async () => {
        // await db.close()
    });

    describe('Create user [POST] /v1/users', () => {
        it('should fail when body is not provided', async () => {
            const response = await request
                .post(api('users'))
                .send({});

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when body is not valid: parameters required missing', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            delete bodyRequestTest.email;
            const response = await request
                .post(api('users'))
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        const testEmail = [
            ['1234', 400, 1],
            ['----', 400, 1],
            ['test.com', 400, 1],
            ['test@', 400, 1],
            ['test@test.notvalid', 400, 1],
        ];
        test.each(testEmail)(
            'should fail when data is not valid. Email=%s statusCode=%s  errorsNum=%s',
            async (email: string, statusCode: number, errorsNum: number) => {
                const bodyRequestTest = _.clone(bodyRequest);
                bodyRequestTest.email = email;
                const response = await request
                    .post(api('users'))
                    .send(bodyRequestTest);

                expect(response.statusCode).toBe(statusCode);
                const { body } = response;
                const { errors } = body;
                expect(errors.length).toBeGreaterThanOrEqual(errorsNum);
            });

        const testPassword = [
            ['Asdf1', 400, 1],
            ['asdf1234', 400, 1],
            ['Asdfasdf', 400, 1],
            ['asdfasdf', 400, 1],
            ['12341234', 400, 1],
            ['Asdf1234.', 400, 1],
        ];
        test.each(testPassword)(
            'should fail when data is not valid. Password=%s statusCode=%s  errorsNum=%s',
            async (password: string, statusCode: number, errorsNum: number) => {
                const bodyRequestTest = _.clone(bodyRequest);
                bodyRequestTest.password = password
                const response = await request
                    .post(api('users'))
                    .send(bodyRequestTest);

                expect(response.statusCode).toBe(statusCode);
                const { body } = response;
                const { errors } = body;
                expect(errors.length).toBeGreaterThanOrEqual(errorsNum);
            });

        it('should fail when user already exists', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            bodyRequestTest.email = `test_${faker.internet.email().toLowerCase()}`;
            // 1. Register user
            await request.post(api('users')).send(bodyRequestTest);
            // 2. Try to register same user
            const response = await request
                .post(api('users'))
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should success', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            bodyRequestTest.email = `test_${faker.internet.email().toLowerCase()}`;
            const response = await request
                .post(api('users'))
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(201);
            const { body } = response;
            expect(body._id).not.toBeNull();
            expect(body.email).not.toBeNull();

            // -----------------
            // Verification data
            // -----------------
            // 1. Verify received user exist
            const user: IUserModel = await UserModel.findById(body._id);
            expect(body._id).not.toBeNull();
            expect(user.email).not.toBeNull();
        });
    });
});
