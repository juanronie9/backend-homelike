import faker from 'faker';
// @ts-ignore
import mongoose from 'mongoose';
import { MongoConnector } from '../../../src/modules/common/mongo-connector';
import applicationConfig from '../../../src/config/application';
import { AuthService } from '../../../src/modules/auth/auth-service';
import { OAuthToken } from '../../../src/modules/auth/oauthtoken';
import { OAuthtokenGateway } from '../../../src/modules/auth/oauthtoken-gateway';
import { UserGateway } from '../../../src/modules/users/user-gateway';
import { User } from '../../../src/modules/users/user';


const TOKEN_TTL_SC = 10 * 60 * 60; // 10h

describe('AuthService', () => {
    beforeAll(async () => {
        const connector = new MongoConnector();
        connector.setConfig(applicationConfig.mongoDb);
        await connector.connect();
    })

    afterAll(async () => {
        // await db.close()
    })

    describe('login', () => {
        it('should fail: missing parameters', async () => {
            try {
                const authService = new AuthService();
                await authService.login('test@test.test', '');
            } catch (e) {
                expect(e.message).toBe('Username or password parameter missing');
            }
        });
        it('should fail: user not found', async () => {
            try {
                const authService = new AuthService();
                await authService.login(faker.internet.email(), 'password');
            } catch (e) {
                expect(e.message).toBe('User not found');
            }
        });
        it('should fail: user password not valid', async () => {
            const email = `test_${faker.internet.email().toLowerCase()}`;
            const name = `test_${faker.datatype.uuid().toLowerCase()}`;
            const password = 'Asdf1234';

            const userGateway = new UserGateway();
            const user = new User();
            user.setEmail(email);
            user.setName(name);
            user.setPassword(password);
            await userGateway.createUser(user);
            try {
                const authService = new AuthService();
                await authService.login(email, 'password-not-valid');
            } catch (e) {
                expect(e.message).toBe('Password not valid');
            }
        });
        it('should return data', async () => {
            const email = `test_${faker.internet.email().toLowerCase()}`;
            const name = `test_${faker.datatype.uuid().toLowerCase()}`;
            const password = 'Asdf1234';

            // 1. Create User
            const userGateway = new UserGateway();
            const user = new User();
            user.setEmail(email);
            user.setName(name);
            user.setPassword(password);
            const responseUser = await userGateway.createUser(user);
            expect(responseUser.email).toBe(email);
            expect(responseUser.name).toBe(name);

            // 2. Login User
            const authService = new AuthService();
            const response = await authService.login(email, password);
            expect(response.email).toBe(email);
        });
    });

    describe('validateToken', () => {
        it('should fail: token not valid', async () => {
            const authService = new AuthService();
            const isValid = await authService.validateToken('');
            expect(isValid).toBe(false);
        });
        it('should return data', async () => {
            const oauthTokenGateway = new OAuthtokenGateway();

            const oauthClientIdBefore = new mongoose.Types.ObjectId();
            const userIdBefore = new mongoose.Types.ObjectId();
            const accessTokenBefore = faker.datatype.uuid();
            const dt = new Date();
            dt.setSeconds( dt.getSeconds() + TOKEN_TTL_SC);
            const accessTokenExpiresAtBefore = dt;

            // 1. Create OAuth Token
            const oauthToken = new OAuthToken();
            oauthToken.setOAuthClientId(oauthClientIdBefore);
            oauthToken.setUserId(userIdBefore);
            oauthToken.setAccessToken(accessTokenBefore);
            oauthToken.setAccessTokenExpiresAt(accessTokenExpiresAtBefore);

            const response = await oauthTokenGateway.createOAuthToken(oauthToken);
            expect(response).not.toBeNull();
            expect(response.id).toBeDefined();

            // 2. Validate Token
            const authService = new AuthService();
            const isValid = await authService.validateToken(response.accessToken);
            expect(isValid).toBe(true);
        });
    });
});