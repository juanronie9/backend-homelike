import { IGetAddressRequestParameters, MapsService } from '../../../src/modules/apartments/maps-service';

describe('MapsService', () => {
    describe('getAddress', () => {
        it('should not return data: not found', async () => {
            const mapsService = new MapsService();
            const obj: IGetAddressRequestParameters = {
                street: '---',
                streetNumber: '---',
                town: '---',
                postalCode: '---',
                country: '---'
            }
            const data = await mapsService.getAddress(obj);
            expect(data).toBeNull();
        });
        it('should return data', async () => {
            const mapsService = new MapsService();
            const obj: IGetAddressRequestParameters = {
                street: 'Alcalá',
                streetNumber: '42',
                town: 'Madrid',
                postalCode: '28014',
                country: 'Spain'
            }
            const data = await mapsService.getAddress(obj);
            expect(data).toBeDefined();
            expect(data).toHaveProperty('lat');
            expect(data).toHaveProperty('lon');
        });
    });
});