// import faker from 'faker';
import { MongoConnector } from '../../../src/modules/common/mongo-connector';
import applicationConfig from '../../../src/config/application';
import mongoose from 'mongoose';
import { Apartment } from '../../../src/modules/apartments/apartment';
import { ApartmentGateway, GetApartmentsParameters, GetApartmentsResult } from '../../../src/modules/apartments/apartment-gateway';

const obj = {
    userId: new mongoose.Types.ObjectId(),
    street: 'Alcalá',
    streetNumber: '42',
    city: 'Madrid',
    country: 'Spain',
    zipcode: '28014',
    rooms: 2,
    location: {
        type: 'Point',
        coordinates: [-3.696513838467072, 40.4182074]
    }
};

describe('ApartmentGateway', () => {
    beforeAll(async () => {
        const connector = new MongoConnector();
        connector.setConfig(applicationConfig.mongoDb);
        await connector.connect();
    })

    afterAll(async () => {
        // await db.close()
    })

    describe('createApartment', () => {
        it('should create apartment', async () => {
            const apartment = new Apartment();
            apartment.setUserId(obj.userId);
            apartment.setStreet(obj.street);
            apartment.setStreetNumber(obj.streetNumber);
            apartment.setCity(obj.city);
            apartment.setCountry(obj.country);
            apartment.setZipcode(obj.zipcode);
            apartment.setRooms(obj.rooms);
            apartment.setLocation(obj.location);

            const apartmentGateway = new ApartmentGateway();
            const response = await apartmentGateway.createApartment(apartment);
            expect(response.id).toBeDefined();
            expect(response.street).toBe(obj.street);
            expect(response.streetNumber).toBe(obj.streetNumber);
            expect(response.city).toBe(obj.city);
            expect(response.country).toBe(obj.country);
            expect(response.zipcode).toBe(obj.zipcode);
            expect(response.rooms).toBe(obj.rooms);
        });
    });

    describe('getApartments', () => {
        const data0: GetApartmentsParameters = {
            page: 999,
            pageSize: 10,
        };
        const data1: GetApartmentsParameters = {
            page: 1,
            pageSize: 10,
            city: obj.city,
            country: obj.country,
            rooms: obj.rooms,
        };
        const data2: GetApartmentsParameters = {
            page: 1,
            pageSize: 10,
            lon: obj.location.coordinates[0],
            lat: obj.location.coordinates[1],
            nearest: 10
        };
        const data3: GetApartmentsParameters = {
            page: 1,
            pageSize: 10,
            lon: -9.9,
            lat: +9.9,
            nearest: 10
        };
        const data4: GetApartmentsParameters = {
            page: 1,
            pageSize: 10,
            city: obj.city,
            country: obj.country,
            rooms: obj.rooms,
            lon: obj.location.coordinates[0],
            lat: obj.location.coordinates[1],
            nearest: 10
        };

        const testData = [
          [data0, 0, 0],
          [data1, 1, 1],
          [data2, 1, 1],
          [data3, 0, 0],
          [data4, 0, 0],
        ];
        test.each(testData)(
            'should return data. Result for getApartments=%s count=%s  rows=%s',
            async (getApartmentsParameters: GetApartmentsParameters, count: number, rows: number) => {
            // 1. Create Data
            const apartment = new Apartment();
            apartment.setUserId(obj.userId);
            apartment.setStreet(obj.street);
            apartment.setStreetNumber(obj.streetNumber);
            apartment.setCity(obj.city);
            apartment.setCountry(obj.country);
            apartment.setZipcode(obj.zipcode);
            apartment.setRooms(obj.rooms);
            apartment.setLocation(obj.location);
            const apartmentGateway = new ApartmentGateway();
            await apartmentGateway.createApartment(apartment);

            // 2. Get Data
            const response: GetApartmentsResult = await apartmentGateway.getApartments(getApartmentsParameters);
            expect(response.count).toBeGreaterThanOrEqual(count);
            expect(response.rows.length).toBeGreaterThanOrEqual(rows);
        });
    });
});